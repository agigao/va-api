# Video Analytics Web Service

Web project for face recognition system - API and SPA.

- run webapp: `lein run`
- access  API through http://localhost:3000

## API
- `/api`     [GET]    get the index
- `/api/`    [POST]   persist an entity
- `/api/:id` [GET]    get a record by id
- `/api/:id` [PUT]    update record by id
- `api/:id`  [DELETE] remove entity by id
- `/request-info`     dumps request for debugging purposes

## Tech stack
- Clojure 1.9
- PostgreSQL(HugSQL)
- Ring/Compojure
- Clojurescript 1.10 (yet to be added)
- re-frame (yet to be added)

## License

Copyright © 2018 Giga Chokheli

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.