-- noinspection SqlNoDataSourceInspectionForFile

-- :name get-persons
-- :doc retrieves all records altogether
SELECT * FROM persons
ORDER BY created_at

-- :name get-person :? :1
-- :doc retrieves a user record given the id
SELECT * FROM persons
WHERE id = :id

-- :name create-person! :<!
-- :doc creates a new person record
INSERT INTO persons
(first_name, last_name, img, face_features)
VALUES (:first_name, :last_name, :img, :face_features)
returning id

-- :name update-person! :! :n
-- :doc updates an existing person record
UPDATE persons
SET first_name = :first_name, last_name = :last_name
WHERE id = :id

-- :name delete-person! :! :n
-- :doc deletes a person record given the id
DELETE FROM persons
WHERE id = :id
