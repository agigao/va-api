(ns va-api.core
  (:require [va-api.handler :refer :all]
            [ring.adapter.jetty :as jetty]
            [ring.util.http-response :as response]
            [compojure.core :refer [defroutes ANY GET PUT POST DELETE context]]
            [compojure.route :as route]
            [ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.format :refer [wrap-restful-format]])
  (:gen-class))

(defroutes api-routes
  (context "/api" []
    (GET "/" [] (list-data))
    (GET "/:id" [id] (get-data id))
    (DELETE "/:id" [id] (delete-entity! id))
    (POST "/" [& req] (create-entity! req))
    (PUT "/:id" [& req] (update-entity! req)))
  (GET "/request-info" req (response/ok req))
  (route/not-found "Well... such uri, doesn't exist, apologies :))\n"))

(def api
  (-> api-routes
      (wrap-restful-format :formats [:json-kw])))

(defn -main [& args]
  (jetty/run-jetty (wrap-reload #'api) {:port 3000 :join? false}))