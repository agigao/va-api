(ns va-api.db
  (:require [hugsql.core :as hugsql]))

(def database
  {:classname   "org.postgresql.Driver"
   :subprotocol "postgresql"
   :subname     "//localhost:5432/va_web_dev"
   :user        "admin"
   :password    "admin"})

(hugsql/def-db-fns "sql/queries.sql" database)

(defn list-data []
  (get-persons))

(defn get-data [id]
  (get-person database id))

(defn create-entity! [entity]
  (create-person! database entity))

(defn update-entity! [entity]
  (update-person! database entity))

(defn delete-entity! [id]
  (delete-person! database id))
