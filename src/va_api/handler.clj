(ns va-api.handler
  (:require [va-api.db :as db]
            [ring.util.http-response :as response]))

(defn respond [data]
  (if (or (nil? data) (= data 0))
    (response/not-found)
    (response/ok data)))

(defn list-data []
  (response/ok (db/list-data)))

(defn get-data [id]
  (respond (db/get-data {:id (Integer/parseInt id)})))

(defn create-entity! [req]
  (respond (db/create-entity! req)))

(defn update-entity! [req]
  ;; converting path-param (string -> int) and assoc-iating with req-map.
  (let [data (assoc req :id (Integer/parseInt (:id req)))]
    (respond (db/update-entity! data))))

(defn delete-entity! [id]
  (respond (db/delete-entity! {:id (Integer/parseInt id)})))
