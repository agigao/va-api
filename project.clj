(defproject va-api "0.1.0-SNAPSHOT"
  :description "Video Analytics API by Thargi"
  :url "Dunno yet"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.postgresql/postgresql "42.2.4"]
                 [com.layerware/hugsql "0.4.5"]
                 [compojure "1.6.1"]
                 [ring "1.6.3"]
                 [metosin/ring-http-response "0.9.0"]
                 [ring-middleware-format "0.7.2"]
                 [ring/ring-json "0.4.0"]]
  :main ^:skip-aot va-api.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
